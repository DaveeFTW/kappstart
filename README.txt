kAppStart is a set of classes that provide easy kernel access on any PSP firmware*. kAppStart is BSD licensed, you can use it in closed source and modify the source. However, we would greatly appreciate that you contribute your improvements!
kAppStart is written in C++, but samples are available to make it easy to use in C too!

[Why do I want to use this?]
Rather than pester around with tweaking exploits to run a signed kernel application, this will do all that work for you. It resolves NIDs, it automatically exploits and it does all this with an easy to use interface. Basically, you can call a kernel nid, or you can start up your own kernel module and link to it.
kAppStart compliments the use of signed modules and can be perfect for custom firmware installers, downgraders, device information, cheat devices and more!

[How can I get a hold of it?]
Sounds to me you've already got it! If not, the source is available here: https://bitbucket.org/DaveeFTW/kappstart You can also fork the code directly into your projects!

[Who do I thank?]
Well, heres the list so far! Contribute and you'll be sitting on this list too!
	-> Davee (davee@x-fusion.co.uk) - kAppStart
	-> some1 - Implementation of 6.39 + 6.60 exploits
	
[Changelog]
	
	30/05/2012 -> Added base code.
