/*
	Firmware class - provides information about the host firmware and relation to exploit
	by Davee
	
	Part of kAppStart
	
	http://lolhax.org
*/
#include <pspkernel.h>

#include "firmware.h"

#include "exploit_639.h"
#include "exploit_660.h"

/**
 *	isSupported accessor method.
 *	return true if firmware is supported, else false
 */
bool Firmware::isFwSupported(void)
{
	/* return if supported */
	return this->is_supported;
}

/**
 *	getDevkit accessor method.
 *	return devkit version
 */
u32 Firmware::getDevkit(void)
{
	/* return the version devkit */
	return this->devkit;
}

/**
 *	getVersion accessor method.
 *	return integer version (0x620, 0x630, 0x631 etc)
 */
u32 Firmware::getFwVersion(void)
{
	/* return the version variable */
	return this->version;
}
		
/**
 *	getString accessor method.
 *	return a pointer to version string
 */
const char *Firmware::getFwString(void)
{
	/* return the version string */
	return this->version_str;
}

Exploit *Firmware::getExploit(void)
{
	/* return object */
	return this->exploit;
}

bool Firmware::findExploit(u32 devkit)
{
	/* ok, lets check for devkit */
	if (devkit == Exploit639::getDevkit())
	{
		/* create an object */
		this->exploit = new Exploit639;
		return true;
	}
	
	// check for 6.60
	else if (devkit == Exploit660::getDevkit())
	{
		// create na object
		this->exploit = new Exploit660;
		return true;
	}
	
	/* no support */
	this->exploit = NULL;
	return false;
}

void Firmware::setDevkit(u32 devkit)
{
	/* lets set some variables */
	this->devkit = devkit;
	this->version = (((devkit >> 24) & 0xF) << 8) | (((devkit >> 16) & 0xF) << 4) | ((devkit >> 8) & 0xF);
	
	/* set the string */
	this->version_str[0] = '0' + ((this->version >> 8) & 0xF);
	this->version_str[1] = '.';
	this->version_str[2] = '0' + ((this->version >> 4) & 0xF);
	this->version_str[3] = '0' + ((this->version >> 0) & 0xF);
	this->version_str[4] = 0;
}

/**
 *	Constructor methods.
 */

Firmware::Firmware(bool exploit_enable, u32 devkit)
{
	/* set the devkit */
	this->setDevkit(devkit);

	/* if exploit stuff */
	if (exploit_enable)
	{
		/* determine if it has an exploit */
		this->is_supported = this->findExploit(this->devkit);
	}
	else
	{
		/* not supported */
		this->is_supported = false;
	}
}

Firmware::~Firmware(void)
{
	/* check for exploit */
	if (this->exploit != NULL)
	{
		/* destroy it */
		delete this->exploit;
	}
}
