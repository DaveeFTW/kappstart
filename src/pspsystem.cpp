/*
	PSP System class - provides automatic determination of exploitation status and provides API to access this
	by Davee
	
	Part of kAppStart
	
	http://lolhax.org
*/
#include <pspkernel.h>

#include "pspsystem.h"
#include "macro.h"


void PSPSystem::enableUnsignedPRX(void)
{
	u32 res;
	
	// get the exploit
	this->getExploit()->execCommand(Exploit::CS_CMD_ENABLE_PRX_LOAD, &res);
}

/**
 *	Constructor method
 *	Specified initial colours, if none specified, will resort to default.
 */
PSPSystem::PSPSystem(void) : Firmware(true, sceKernelDevkitVersion())
{
	/* check if firmware is setup correctly */
	if (this->isFwSupported())
	{
		/* setup model */
		this->setupModel(this->getExploit());
	}
}

PSPSystem::~PSPSystem(void)
{

}
