/*
	Example class - provides an enduser interface for downgrading a PSP system.
	by Davee

	Part of kAppStart
	
	http://lolhax.org
*/

#ifndef __EXAMPLE_H__
#define __EXAMPLE_H__

#include "application.h"

class Example : public Application
{
	public:
		/* constructor / destructor */
		Example(void);
		
		/* main method */
		int start(void);
		
	private:
		// constants
		static const u32 FG_COLOUR = 0x00D05435;
		static const u32 BG_COLOUR = 0x00000000;
		
		// methods
		void displayVersion(void);
};

#endif /* __EXAMPLE_H__ */
