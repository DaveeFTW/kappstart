/*
	Application class - describes common functionality for an application.
	by Davee

	Part of kAppStart
	
	http://lolhax.org
*/

#include <pspkernel.h>
#include <stdio.h>
#include <stdarg.h>
#include "application.h"

int Application::loadStartPRX(const char *file)
{
	// load our PRX
	SceUID modid = sceKernelLoadModule(file, 0, NULL);
	
	// check errors
	if (modid < 0)
	{
		// error loading prx
		error(5000, "Error 0x%08X loading %s\n", modid, file);
		return -3;
	}
	
	// start module
	int res = sceKernelStartModule(modid, 0, NULL, NULL, NULL);
	
	// check errors
	if (res < 0)
	{
		// error starting prx
		error(5000, "Error 0x%08X starting %s\n", res, file);
		return -4;
	}
	
	// success!
	return 0;
}

void Application::error(u32 wait_ms, const char *fmt, ...)
{
	char msg[256];
	va_list list;
	
	// sort out arguement list
	va_start(list, fmt);
	vsprintf(msg, fmt, list);
	va_end(list);
	
	// get previous colour
	u32 colour = getForegroundColour();
	
	// change colour to red
	setForegroundColour(DISPLAY_COLOUR_ERR);
	printf(msg);
	
	// set old colour
	setForegroundColour(colour);
	
	// wait
	sceKernelDelayThread(wait_ms * 1000);
	
	// exit
	sceKernelExitGame();
}
