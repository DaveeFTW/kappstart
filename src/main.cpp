/*
	main.cpp -> code initialisation
	by Davee
	
	Part of kAppStart
	
	http://lolhax.org
*/
#include <pspkernel.h>
#include <stdio.h>
#include "example.h"

PSP_MODULE_INFO("Example", 0, 1, 0);

int main(int argc, char *argv[])
{                    
	// create object
	Example app;
	
	// start the application
	int res = app.start();
	
	// end the program
	printf("Program termination. code = %i\n", res);
	
	// exit
	sceKernelExitGame();
	return res;
}
