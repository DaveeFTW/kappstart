/*
	Firmware class - provides information about the host firmware and relation to exploit
	by Davee
	
	Part of kAppStart
	
	http://lolhax.org
*/
#ifndef __FIRMWARE_H__
#define __FIRMWARE_H__

#include <pspkernel.h>
#include "exploit.h"

class Firmware
{
	public:
		/* constructor. default devkit of system unless provided */
		Firmware(bool exploit_enable = false, u32 devkit = sceKernelDevkitVersion());
		~Firmware(void);
		
		/* return true if supported, else false */
		bool isFwSupported(void);
		
		/* accessor methods for the version number */
		u32 getDevkit(void);
		u32 getFwVersion(void);
		const char *getFwString(void);
		
	protected:
		Exploit *getExploit(void);
		void setDevkit(u32 devkit);
		
	private:
		/* private variables */
		u32 devkit;
		u32 version;
		bool is_supported;
		char version_str[5];
		
		Exploit *exploit;
		
		/* checks list of supported firmwares and returns true is found. */
		bool findExploit(u32 devkit);
};

#endif /* __FIRMWARE_H__ */
