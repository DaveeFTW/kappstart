/*
	Example class - provides an enduser interface for downgrading a PSP system.
	by Davee

	Part of kAppStart
	
	http://lolhax.org
*/

#include <pspkernel.h>
#include <stdio.h>
#include <stdarg.h>

#include "example.h"
#include "pspsystem.h"

void Example::displayVersion(void)
{
	// display the version and date built
	printf(
		"Example Application" "\n"
		"(C) Davee 2012. http://lolhax.org" "\n"
		"Version %s. Built %s %s" "\n" "\n"
		, APP_VERSION, __DATE__, __TIME__);
}

int Example::start(void)
{
	// Create PSPSystem object
	PSPSystem system;
	
	// display program info
	displayVersion();
	
	// display the fw version we got
	printf("Detected firmware %s\n", system.getFwString());
	
	// check if fw is supported
	if (!system.isFwSupported())
	{
		// unsupported firmware
		error(5000, "Your firmware is not supported!");
		return -1;
	}
	
	// display the model we got
	printf("Detected model %s\n", system.getModelString());
	
	// check if supported
	if (!system.isModelSupported())
	{
		// unsupported model
		error(5000, "Your model is not supported!");
		return -2;
	}
	
	sceKernelSleepThread();
	return 0;
}

Example::Example(void) : Application(FG_COLOUR, BG_COLOUR)
{

}
