/*
	Display class - provides an interface for displaying messages on the screen
	by Davee

	Part of kAppStart
	
	http://lolhax.org
*/

#ifndef __DISPLAY_H__
#define __DISPLAY_H__

class Display
{
	public:
		// constructor / destructor
		Display(u32 fg_colour = DISPLAY_DEFAULT_FG, u32 bg_colour = DISPLAY_DEFAULT_BG);
		
		// Colour tools
		void setForegroundColour(u32 colour);
		void setBackgroundColour(u32 colour);
		u32 getForegroundColour(void);
		u32 getBackgroundColour(void);
		
		// display tools
		int print(const char *str);
		int printf(const char *fmt, ...);
		
		// clear screen
		void clearScreen(void);
		
		// constants
		static const u32 DISPLAY_COLOUR_ERR = 0x000000FF;
		
	private:
		//constants - white text, black background
		static const u32 DISPLAY_DEFAULT_FG = 0x00FFFFFF;
		static const u32 DISPLAY_DEFAULT_BG = 0x00000000;
		
		// need to store the colours
		u32 fg_colour, bg_colour;
};

#endif /* __DISPLAY_H__ */
