/*
	PSP System class - provides automatic determination of exploitation status and provides API to access this
	by Davee
	
	Part of kAppStart
	
	http://lolhax.org
*/
#include "firmware.h"
#include "model.h"

class PSPSystem : public Firmware, public Model
{
	public:
		/* constructor / destructor */
		PSPSystem(void);
		~PSPSystem(void);
	
		/* methods */
		void enableUnsignedPRX(void);
		
	private:
		
};
