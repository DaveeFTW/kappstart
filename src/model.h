/*
	Model class - provides information about the host psp model
	by Davee
	
	Part of kAppStart
	
	http://lolhax.org
*/
#ifndef __MODEL_H__
#define __MODEL_H__

#include "exploit.h"

class Model
{
	public:
		// constructors
		Model(void);
		Model(Exploit *exploit);
		
		// query functions
		bool isModelSupported(void);
		const char *getModelString(void);
		const char *getModelStringBaryon(void);
		
		// version info
		u32 getModel(void);
		u32 getModelBaryon(void);
		
		// constants
		enum Models
		{
			PSP_PHAT = 0,
			PSP_SLIM,
			PSP_3000,
			PSP_3000_04G,
			PSP_GO,
			PSP_3000_07G = 6,
			PSP_3000_09G = 8,
			PSP_STREET = 10
		};
		
	protected:
		// sets up a model in relation to exploit
		void setupModel(Exploit *exploit);
		
	private:
		Exploit *exploit;
		bool is_supported;
		u32 model;
		u32 baryon;
		
		bool queryBaryon(void);
		bool queryModel(void);
		
		u32 convertBaryon(u32 baryon);
};

#endif /* __MODEL_H__ */
