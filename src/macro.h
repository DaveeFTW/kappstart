/*
	Macros - provides useful macros for various things
	by Davee
	
	Part of kAppStart
	
	http://lolhax.org
*/
#ifndef __MACRO_H__
#define __MACRO_H__


#define MIPS_JAL(f)						(0x0C000000 | (((u32)(f) >> 2)  & 0x03ffffff))
#define MIPS_J(f)						(0x08000000 | (((u32)(f) & 0x0ffffffc) >> 2))
#define MAKE_CALL(a, f)					_sw(0x0C000000 | (((u32)(f) >> 2)  & 0x03ffffff), a)
#define MAKE_JUMP(a, f)					_sw(0x08000000 | (((u32)(f) & 0x0ffffffc) >> 2), a)
#define REDIRECT_FUNCTION(a, f) 		{ u32 address = a; _sw(0x08000000 | (((u32)(f) >> 2)  & 0x03ffffff), address);  _sw(0, address+4); }
#define MAKE_RELATIVE_BRANCH(a, f, t)	_sw((0x10000000 | ((((f - a) >> 2) - 1) & 0xFFFF)), a + t)

#define PSP_DEVKIT_STR(s)	((((s[0] - '0') & 0xFF) << 24) | (((s[2] - '0') & 0xFF) << 16) | (((s[3] - '0') & 0xFF) << 8) | 0x10)
#define PSP_DEVKIT(x) ((((((unsigned int)(x * 100) / 100) % 10) & 0xF) << 24) | (((((unsigned int)(x * 100) / 10) % 10) & 0xF) << 16) | (((((unsigned int)(x * 100) / 1) % 10) & 0xF) << 8) | 0x10)

#define PSPGO_UPDATER		"ef0:/PSP/GAME/UPDATE/EBOOT.PBP"
#define STANDARD_UPDATER	"ms0:/PSP/GAME/UPDATE/EBOOT.PBP"


#endif /* __MACRO_H__ */
