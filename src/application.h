/*
	Application class - describes common functionality for an application.
	by Davee

	Part of kAppStart
	
	http://lolhax.org
*/

#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include "display.h"

class Application: protected Display
{
	public:
		Application() {};
		Application(u32 fg, u32 bg) : Display(fg, bg) {};
		
		virtual int start(void) = 0;
		
	protected:
		// load and start a PRX
		int loadStartPRX(const char *file);
		
	protected:
		void error(u32 wait_ms, const char *fmt, ...);
};

#endif /* __APPLICATION_H__ */
