/*
	Display class - provides an interface for displaying messages on the screen
	by Davee

	Part of kAppStart
	
	http://lolhax.org
*/

#include <pspkernel.h>

#include <stdio.h>
#include <stdarg.h>

#include "display.h"

/**
 *	printf method.
 *	Use this to display a null terminated string.
 */
int Display::printf(const char *fmt, ...)
{
	char msg[256];
	va_list list;

	/* sort our argument list */
	va_start(list, fmt);
	vsprintf(msg, fmt, list);
	va_end(list);

	/* wrap sdk */
	return pspDebugScreenPuts(msg);
}

/**
 *	print method.
 *	Use this to display a null terminated string.
 */
int Display::print(const char *str)
{
	/* wrap sdk */
	return pspDebugScreenPuts(str);
}

/**
 *	setForegroundColour method.
 *	Use this to set the text colour
 */
void Display::setForegroundColour(u32 colour)
{
	// set our stored variable
	fg_colour = colour;
	
	/* wrap the psp sdk */
	pspDebugScreenSetTextColor(colour);
}

/**
 *	setBackgroundColour method.
 *	Use this to set the background for the text display
 */
void Display::setBackgroundColour(u32 colour)
{
	// set our stored variable
	bg_colour = colour;
	
	/* wrap the psp sdk */
	pspDebugScreenSetBackColor(colour);
}

/**
 *	getForegroundColour method.
 *	Use this to get the foreground for the text display
 */
u32 Display::getForegroundColour(void)
{
	// get the foreground colour
	return fg_colour;
}

/**
 *	getBackgroundColour method.
 *	Use this to get the background for the text display
 */
u32 Display::getBackgroundColour(void)
{
	// get the background colour
	return bg_colour;
}

/**
 *	clearScreen method.
 *	Use this to clear the screen
 */
void Display::clearScreen(void)
{
	/* clear the screen */
	pspDebugScreenClear();
}

/**
 *	Constructor method
 *	Specified initial colours, if none specified, will resort to default.
 */
Display::Display(u32 fg_colour, u32 bg_colour)
{
	/* init the "debug" screen */
	pspDebugScreenInit();
	
	/* set the default colours */
	this->setForegroundColour(fg_colour);
	this->setBackgroundColour(bg_colour);
	
	/* clear screen */
	this->clearScreen();
}
