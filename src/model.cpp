/*
	Model class - provides information about the host psp model
	by Davee
	
	Part of kAppStart
	
	http://lolhax.org
*/
#include <pspkernel.h>
#include "model.h"

bool Model::queryModel(void)
{
	/* create a call to kernel */
	u32 args[2];
	
	/* ok, set args */
	args[0] = (u32)"sceKernelGetModel"; //function
	args[1] = 0; //number of args
	
	/* do call */
	if (this->exploit->execCommand(Exploit::CS_CMD_NID, args))
	{
		/* success! */
		this->model = args[0];
		return true;
	}
	
	/* failed */
	return false;
}

bool Model::queryBaryon(void)
{
	/* create a call to kernel */
	u32 args[3];
	u32 baryon = 0;
	
	/* ok, set args */
	args[0] = (u32)"sceSysconGetBaryonVersion"; //function 
	args[1] = 1; //number of args
	args[2] = (u32)&baryon; //arg
	
	/* do call */
	if (this->exploit->execCommand(Exploit::CS_CMD_NID, args))
	{
		/* success! */
		this->baryon = baryon;
		return true;
	}
	
	/* failed */
	return false;
}

u32 Model::getModelBaryon(void)
{
	/* return the baryon version */
	return this->convertBaryon(this->baryon);
}

u32 Model::getModel(void)
{
	/* return the model */
	return this->model;
}

const char *Model::getModelStringBaryon(void)
{
	const char *str = NULL;
	
	/* lets get the model */
	switch (this->getModelBaryon())
	{
		/* PSP Phat */
		case PSP_PHAT: str = "01g PSP Phat"; break;
		
		/* PSP Slim */
		case PSP_SLIM: str = "02g PSP Slim"; break;
		
		/* PSP 3000 */
		case PSP_3000: str = "03g PSP 3000"; break;
		
		/* PSP 3000 04g */
		case PSP_3000_04G: str = "04g PSP 3000"; break;
		
		/* PSP 3000 07g */
		case PSP_3000_07G: str = "07g PSP 3000"; break;
		
		/* PSP 3000 09g */
		case PSP_3000_09G: str = "09g PSP 3000"; break;
		
		/* PSP Street 11g */
		case PSP_STREET: str = "11g PSP Street"; break;
		
		/* PSP Go */
		case PSP_GO: str = "05g PSP GO"; break;
		
		default:
		{
			/* unknown ... spooky */
			str = "Unknown baryon";
			break;
		}
	}
	
	/* return string */
	return str;
}

const char *Model::getModelString(void)
{
	const char *str = NULL;
	
	/* lets get the model */
	switch (this->getModel())
	{
		/* PSP Phat */
		case PSP_PHAT: str = "01g PSP Phat"; break;
		
		/* PSP Slim */
		case PSP_SLIM: str = "02g PSP Slim"; break;
		
		/* PSP 3000 */
		case PSP_3000: str = "03g PSP 3000"; break;
		
		/* PSP 3000 04g */
		case PSP_3000_04G: str = "04g PSP 3000"; break;
		
		/* PSP 3000 07g */
		case PSP_3000_07G: str = "07g PSP 3000"; break;
		
		/* PSP 3000 09g */
		case PSP_3000_09G: str = "09g PSP 3000"; break;
		
		/* PSP Street 11g */
		case PSP_STREET: str = "11g PSP Street"; break;
		
		/* PSP Go */
		case PSP_GO: str = "05g PSP GO"; break;
		
		default:
		{
			/* unknown ... spooky */
			char *str2 = (char *)"XXg Unknown";
			str2[0] = (this->model / 10) % 10;
			str2[1] = (this->model) % 10;
			
			/* nasty me skipping the type definition */
			str = (const char *)str2;
			break;
		}
	}
	
	/* return the string */
	return str;
}

u32 Model::convertBaryon(u32 baryon)
{
	/* get upper byte of version */
	u8 ver_data = (baryon >> 16) & 0xFF;
	
	/* select the motherboard revision */
	switch (ver_data >> 4)
	{
		/* 01g motherboard */
		case 0:
		case 1:
		{
			/* PSP phat */
			return PSP_PHAT;
		}
		
		/* Slim+3000 motherboards */
		case 2:
		{
			/* 02g motherboard */
			if (ver_data >= 0x22 && ver_data < 0x26)
			{
				return PSP_SLIM;
			}
			
			/*  psp 3000 */
			else if (ver_data >= 0x26 && ver_data < 0x29)
			{
				/* PSP 03g */
				return PSP_3000;
			}
			
			/* psp 3000 04g */
			else if (ver_data >= 0x29 && ver_data < 0x2E)
			{
				/* 04g */
				return PSP_3000_04G;
			}
			
			/* psp 3000 07g/09g */
			else if (ver_data >= 0x2E && ver_data < 0x30)
			{
				/* check for 07g */
				if (((baryon >> 24) & 0xFF) == 1)
				{
					/* 07g */
					return PSP_3000_07G;
				}
				
				/* 09g */
				return PSP_3000_09G;
			}
		}
		
		/* PSPgo Boards */
		case 3:
		{
			/* (05g) */
			if (ver_data >= 0x30 && ver_data < 0x32)
			{
				return PSP_GO;
			}	
		}
		
		case 4:
		{
			/* should be PSP Street */
			return PSP_STREET;
		}
	}

	/* shit. */
	return -1;

}

void Model::setupModel(Exploit *exploit)
{
	/* set exploit class */
	this->exploit = exploit;
	
	/* query model */
	this->queryModel();
	
	/* query baryon */
	this->queryBaryon();
	
	/* check if supported */
	this->is_supported = this->exploit->isSupportedModel(this->model);
}

bool Model::isModelSupported(void)
{
	/* return boolean */
	return this->is_supported;
}

Model::Model(Exploit *exploit)
{
	/* setup the model */
	this->setupModel(exploit);
}

Model::Model(void)
{
	/* set to default unsupported */
	this->is_supported = false;
	this->exploit = NULL;
}

