/*
 * PSP Software Development Kit - http://www.pspdev.org
 * -----------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in PSPSDK root for details.
 *
 * psploadcore.h - Interface to LoadCoreForKernel.
 *
 * Copyright (c) 2005 Marcus R. Brown <mrbrown@ocgnet.org>
 * Copyright (c) 2005 James Forshaw <tyranid@gmail.com>
 * Copyright (c) 2005 John Kelley <ps2dev@kelley.ca>
 *
 * $Id: psploadcore.h 1095 2005-09-27 21:02:16Z jim $
 */

#ifndef PSPLOADCORE_H
#define PSPLOADCORE_H

#include <pspkerneltypes.h>

/** @defgroup LoadCore Interface to the LoadCoreForKernel library.
 */

#ifdef __cplusplus
extern "C" {
#endif

/* M33 TAG */
#define M33_KERNEL_EXEC_TAG				(0x55668D96)
#define M33_USER_MS_GAME_TAG			(0x8555ABF2)

#define SceModule2 SceModule
/** @addtogroup LoadCore Interface to the LoadCoreForKernel library. */
/*@{*/

/** Describes a module.  This structure could change in future firmware revisions. */
typedef struct SceModule
{
	struct SceModule*	next; //0, 0x00
	u16 				attribute; //4, 0x04
	u8 					version[2]; //6, 0x06
	char 				modname[27]; //8, 0x08
	char 				terminal; //35, 0x23
	u16 				status; //36, 0x24 (Used in modulemgr for stage identification)
	u16 				unk_26; //38, 0x26
	u32 				unk_28; //40, 0x28
	SceUID 				modid; //44, 0x2C
	SceUID 				usermod_thid; //48, 0x30
	SceUID 				memid; //52, 0x34
	SceUID 				mpidtext; //56, 0x38
	SceUID 				mpiddata; //60, 0x3C
	void *				ent_top; //64, 0x40
	u32 				ent_size; //68, 0x44
	void *				stub_top; //72, 0x48
	u32 				stub_size; //76, 0x4C
	int 				(* module_start)(SceSize, void *); //80, 0x50
	int 				(* module_stop)(SceSize, void *); //84, 0x54
	int 				(* module_bootstart)(SceSize, void *); //88, 0x58
	int 				(* module_reboot_before)(void *); //92, 0x5C
	int 				(* module_reboot_phase)(int reboot_stage, void *reboot_param, u32 unk, u32 unk_2); //96, 0x60 (3 = user reboot before dispatched, 2 = pre-user threads killed, 1 = user threads killed, - pre kernel reboot before dispatched)
	u32 				entry_addr; //100, 0x64(seems to be used as a default address)
	u32 				gp_value; //104, 0x68
	u32 				text_addr; //108, 0x6C
	u32 				text_size; //112, 0x70
	u32 				data_size; //116, 0x74
	u32 				bss_size; //120, 0x78
	u8 					nsegment; //124, 0x7C
	u8					padding[3]; //125, 0x7D
	u32 				segmentaddr[4]; //128, 0x80
	u32 				segmentsize[4]; //144, 0x90
	int 				module_start_thread_priority; //160, 0xA0
	SceSize 			module_start_thread_stacksize; //164, 0xA4
	SceUInt 			module_start_thread_attr; //168, 0xA8
	int 				module_stop_thread_priority; //172, 0xAC
	SceSize 			module_stop_thread_stacksize; //176, 0xB0
	SceUInt 			module_stop_thread_attr; //180, 0xB4
	int 				module_reboot_before_thread_priority; //184, 0xB8
	SceSize 			module_reboot_before_thread_stacksize; //188, 0xBC
	SceUInt 			module_reboot_before_thread_attr; //192, 0xC0
} SceModule;

// For 1.50+

/** Defines a library and its exported functions and variables.  Use the len
    member to determine the real size of the table (size = len * 4). */
typedef struct SceLibraryEntryTable {
	/**The library's name. */
	const char *		libname; //0
	/** Library version. */
	unsigned char		version[2]; //4
	/** Library attributes. */
	signed short		attribute; //6
	/** Length of this entry table in 32-bit WORDs. */ 
	unsigned char		len; //8
	/** The number of variables exported by the library. */
	unsigned char		vstubcount; //9
	/** The number of functions exported by the library. */
	unsigned short		stubcount; //10
	/** Pointer to the entry table; an array of NIDs followed by
	    pointers to functions and variables. */
	void *				entrytable; //12
} SceLibraryEntryTable;

/** Specifies a library and a set of imports from that library.  Use the len
    member to determine the real size of the table (size = len * 4). */
typedef struct SceLibraryStubTable {
	/* The name of the library we're importing from. */
	const char *		libname;
	/** Minimum required version of the library we want to import. */
	unsigned char		version[2];
	/* Import attributes. */
	unsigned short		attribute;
	/** Length of this stub table in 32-bit WORDs. */
	unsigned char		len;
	/** The number of variables imported from the library. */
	unsigned char		vstubcount;
	/** The number of functions imported from the library. */
	unsigned short		stubcount;
	/** Pointer to an array of NIDs. */
	unsigned int *		nidtable;
	/** Pointer to the imported function stubs. */
	void *				stubtable;
	/** Pointer to the imported variable stubs. */
	void *				vstubtable;
} SceLibraryStubTable;

/*typedef struct
{
	u16 mod_attr; //0
	u16 unk_2; //2
	char modname[28]; //4 (includes the terminating character)
	u32 module_gp; //32
	void *ent_top; //36
	void *ent_end; //40
	void *stub_top; //44
	void *stub_end; //48
} SceModuleInfo;*/

typedef struct
{
	u32 unk_0;
	u32 mode_attribute; //4 (mode attribute to check (0 = for decrypt, 1 = for header parse w/ compression, 2 for no compression check))
	u32 api_type; //8 (The api-type)
	u32 unk_12;
	u32 exec_size; //16 (The size of the executable (including header))
	u32 max_alloc_size; //20 (the maximium size needed for the decompression)
	u32 decompression_memid; //24 (the memid for the decompression buffer)
	void *filebase; //28 (pointer to the module data to manipulate)
	u32 elf_type; //32 (the elf_type, if is (1 then it is PRX, 3 = ELF))
	void *topaddr; //36 (the text_addr segment of the executable (in memory))
	u32 entry_addr; //40 (the address of which the code will initially execute into)
	u32 unk_44;
	u32 largest_seg_size; //48 (the size of the largest segment (should be text_size, but technically can be anything)
	u32 text_size; //52 (the size of the text section)
	u32 data_size; //56 (the size of the data section)
	u32 bss_size; //60 (the size of the bss section)
	u32 partition_id; //64 (the memory partition where this belongs to)
	u32 is_kernel_mod; //68 (set to 1 if kernel module else 0 for user)
	u32 is_decrypted; //72 (set to 1 if successfully decrypted)
	u32 moduleinfo_offset; //76 (the offset to the module info section)
	SceModuleInfo *moduleinfo; //80 (pointer to the module info)
	u32 is_compressed; //84 (set to 1 if module is compressed)
	u16 modinfo_attribute; //88 (The module attribute (0x1000 for kernel 0x0000 for user etc))
	u16 exec_attribute; //90 (the executable's attribute (1 for compression, 2 for ELF, 0x200 for KL4E compression else GZIP, 0x8 for GZIP overlap ))
	u32 dec_size; //92 (the size of the module decompressed (including header))
	u32 is_decompressed; //96 (set to 1 if decompression was successful)
	u32 is_signchecked; //100 (set to 1 if the executable was signchecked)
	u32 unk_104;
	u32 overlap_size; //108 (the size of the gzip overlap)
	void *exports_info; //112 (pointer to the exports info)
	u32 exports_size; //116 (the size of the exports)
	void *imports_info; //120 (pointer to the imports info)
	u32 imports_size; //124 (the size of the imports)
	void *strtab_offset; //128 (pointer to the strtab section)
	u8 nsegments; //132 (the number of segments in the executable)
	u8 padding[3]; //133 (unused)
	u32 segment_addr[4]; //136
	u32 segment_size[4]; //152
	u32 unk_168;
	u32 unk_172;
	u32 unk_176;
	u32 unk_180;
	u32 unk_184;
	u32 max_seg_align; //188 (The largest value of the seg_align array)
} SceLoadCoreExecFileInfo;

typedef struct
{
	u32 signature; 		//0
	u16 mod_attribute; 	//4
	u16 comp_attribute;	//6
	u8 module_ver_lo;	//8
	u8 module_ver_hi;	//9
	char modname[28];	//0xA
	u8 mod_version;		//0x26
	u8 nsegments;		//0x27
	u32 elf_size;		//0x28
	u32 psp_size;		//0x2C
	u32 boot_entry;		//0x30
	u32 modinfo_offset; //0x34
	int bss_size;		//0x38
	u32 seg_align[4];	//0x3C
	u32 seg_address[4];	//0x44
	int seg_size[4];	//0x54
	u32 reserved[5];	//0x64
	u32 devkit_version;	//0x78
	u8 decrypt_mode;	//0x7C
	u8 padding;			//0x7D
	u8 overlap_size;	//0x7E
	u8 key_data[0x30];	//0x80
	u32 comp_size;		//0xB0
	int _80;			//0xB4
	u32 unk_B8;			//0xB8
	u32 unk_BC;			//0xBC
	u8 key_data2[0x10];	//0xC0
	u32 tag;			//0xD0
	u8 scheck[0x58];	//0xD4
	u8 sha1_hash[0x14];	//0x12C
	u8 key_data4[0x10]; //0x140
} PSP_Header2; //0x150

typedef struct
{
	u32		signature;  // 0
	u16		attribute; // 4  modinfo
	u16		comp_attribute; // 6
	u8		module_ver_lo;	// 8
	u8		module_ver_hi;	// 9
	char	modname[28]; // 0A
	u8		version; // 26
	u8		nsegments; // 27
	int		elf_size; // 28
	int		psp_size; // 2C
	u32		entry;	// 30
	u32		modinfo_offset; // 34
	int		bss_size; // 38
	u16		seg_align[4]; // 3C
	u32		seg_address[4]; // 44
	int		seg_size[4]; // 54
	u32		reserved[5]; // 64
	u32		devkitversion; // 78
	u8		decrypt_mode; // 7C 
	u8		padding; //7D
	u16		overlap_size; //7E
	u8		key_data0[0x30]; // 80
	int		comp_size; // B0
	int		_80;	// B4
	int		reserved2[2];	// B8
	u8		key_data1[0x10]; // C0
	u32		tag; // D0
	u8		scheck[0x58]; // D4
	u32		key_data2; // 12C
	u32		oe_tag; // 130
	u8		key_data3[0x1C]; // 134
} PSP_Header;

/**
 * Find a module by it's name.
 *
 * @param modname - The name of the module.
 *
 * @returns Pointer to the ::SceModule structure if found, otherwise NULL.
 */
//SceModule * sceKernelFindModuleByName(const char *modname);
SceModule *sceKernelFindModuleByName(const char *modname);

/**
 * Find a module from an address.
 *
 * @param addr - Address somewhere within the module.
 *
 * @returns Pointer to the ::SceModule structure if found, otherwise NULL.
 */
//SceModule * sceKernelFindModuleByAddress(unsigned int addr);
SceModule *sceKernelFindModuleByAddress(unsigned int addr);

/**
 * Find a module by it's UID.
 *
 * @param modid - The UID of the module.
 *
 * @returns Pointer to the ::SceModule structure if found, otherwise NULL.
 */
//SceModule * sceKernelFindModuleByUID(SceUID modid);
SceModule *sceKernelFindModuleByUID(SceUID modid);


/**
 * Return the count of loaded modules.
 *
 * @returns The count of loaded modules.
 */
int sceKernelModuleCount(void);

/**
 * Invalidate the CPU's instruction cache.
 */
void sceKernelIcacheClearAll(void);

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /* PSPLOADCORE_H */
